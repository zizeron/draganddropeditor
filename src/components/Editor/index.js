import React from 'react';
import PropTypes from 'prop-types';

import SlateEditor from './Slate';


const Editor = (props) => {
  const {
    all,
    blockButtons,
    blockData,
    buttonButton,
    columnsThree,
    columnsTwo,
    edit,
    hotKeys,
    imageButton,
    layoutBlocks,
    linkButton,
    markButtons,
    onKeyDown,
    videoButton,
    headerButtons,
  } = props;


  return (
    <SlateEditor
      all={all}
      blockButtons={blockButtons}
      blockData={blockData}
      buttonButton={buttonButton}
      columnsThree={columnsThree}
      columnsTwo={columnsTwo}
      hotKeys={hotKeys}
      imageButton={imageButton}
      layoutBlocks={layoutBlocks}
      linkButton={linkButton}
      markButtons={markButtons}
      onKeyDown={onKeyDown}
      readOnly={!edit}
      videoButton={videoButton}
      headerButtons={headerButtons}
    />
  );
};

Editor.propTypes = {
  all: PropTypes.bool,
  blockButtons: PropTypes.bool,
  blockData: PropTypes.object.isRequired,
  buttonButton: PropTypes.bool,
  columnsThree: PropTypes.bool,
  columnsTwo: PropTypes.bool,
  edit: PropTypes.bool.isRequired,
  headerButtons: PropTypes.bool,
  hotKeys: PropTypes.bool,
  imageButton: PropTypes.bool,
  layoutBlocks: PropTypes.array.isRequired,
  linkButton: PropTypes.bool,
  markButtons: PropTypes.bool,
  onKeyDown: PropTypes.bool,
  videoButton: PropTypes.bool,
};

Editor.defaultProps = {
  all: false,
  blockButtons: false,
  buttonButton: false,
  columnsThree: false,
  columnsTwo: false,
  headerButtons: false,
  hotKeys: false,
  imageButton: false,
  linkButton: false,
  markButtons: false,
  onKeyDown: false,
  videoButton: false,
};

export default Editor;
