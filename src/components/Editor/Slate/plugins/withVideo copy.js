import { Transforms } from 'slate';
import isUrl from 'is-url';


const insertVideo = (editor, url) => {
  const text = { text: '' };
  const video = { type: 'video', url, children: [text] };
  Transforms.insertNodes(editor, [video, { type: 'paragraph', children: [{ text: '' }] }]);
};


const withVideo = (editor) => {
  const { insertData, isVoid } = editor;

  editor.isVoid = (element) => {
    return element.type === 'video' ? true : isVoid(element);
  };

  editor.insertData = (data) => {
    const text = data.getData('text/plain');

    if (text.url && isUrl(text.url)) {
      insertVideo(editor, text.url);
    } else {
      insertData(data);
    }
  };

  return editor;
};


export { insertVideo, withVideo };
