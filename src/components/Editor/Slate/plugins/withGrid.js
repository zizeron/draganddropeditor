import { Transforms, Path } from 'slate';

const gridTwo = {
  type: 'grid',
  columns: 2,
  children: [
    {
      type: 'container',
      columns: 2,
      children: [
        {
          type: 'paragraph',
          children: [{ text: 'Area 1' }],
        },
      ],
    },
    {
      type: 'container',
      columns: 2,
      children: [
        {
          type: 'paragraph',
          children: [{ text: 'Area 2' }],
        },
      ],
    },
  ],
};


const gridThree = {
  type: 'grid',
  columns: 3,
  children: [
    {
      type: 'container',
      columns: 3,
      children: [
        {
          type: 'paragraph',
          children: [{ text: 'Area 1' }],
        },
      ],
    },
    {
      type: 'container',
      columns: 3,
      children: [
        {
          type: 'paragraph',
          children: [{ text: 'Area 2' }],
        },
      ],
    },
    {
      type: 'container',
      children: [
        {
          type: 'paragraph',
          children: [{ text: 'Area 3' }],
        },
      ],
    },
  ],
};


const insertGrid = (editor, columns) => {
  const gridNode = columns == 2 ? gridTwo : gridThree;
  Transforms.insertNodes(editor, [gridNode, { type: 'paragraph', children: [{ text: '' }] }]);
};


const withGrid = (editor) => {
  const { isVoid } = editor;

  editor.isVoid = (element) => {
    return element.type === 'grid' ? true : isVoid(element);
  };

  editor.isVoid = (element) => {
    return element.type === 'container' ? true : isVoid(element);
  };

  return editor;
};


export { gridTwo, gridThree, withGrid, insertGrid };
