import { Editor, Transforms } from 'slate';
import isUrl from 'is-url';


const insertVideo = (editor, url) => {
  const video = {
    type: 'figure',
    children: [
      { type: 'video', url, children: [{ text: '' }] },
      { type: 'figcaption', children: [{ text: '' }] },
    ],
  };

  Transforms.insertNodes(editor, [video, { type: 'paragraph', children: [{ text: '' }] }]);
};


function getAboveBlockType(editor) {
  const tmp = Editor.above(editor, {
    match: (n) => Editor.isBlock(editor, n),
  });
  return tmp && tmp[0].type;
}

const withVideo = (editor) => {
  const { insertBreak, insertData, isVoid } = editor;

  editor.isVoid = (element) => {
    return element.type === 'video' ? true : isVoid(element);
  };

  editor.insertBreak = () => {
    const type = getAboveBlockType(editor);
    insertBreak();
    if (type === 'figcaption') {
      Transforms.liftNodes(editor);
    }
  };

  editor.insertData = (data) => {
    const text = data.getData('text/plain');

    if (text.url && isUrl(text.url)) {
      insertVideo(editor, text.url);
    } else {
      insertData(data);
    }
  };

  return editor;
};


export { insertVideo, withVideo };
