import { Transforms, Editor } from 'slate';


const initialButton = [
  {
    type: 'div',
    className: 'button-container',
    children: [
      {
        type: 'paragraph',
        children: [{ text: '' }],
      },
    ],
  },
];


function getAboveBlockType(editor) {
  const tmp = Editor.above(editor, {
    match: (n) => Editor.isBlock(editor, n),
  });
  return tmp && tmp[0].type;
}


const insertButton = (editor, url, text) => {
  const button = {
    type: 'button',
    url,
    children: [{ text }],
  };

  Transforms.insertNodes(editor, [button, { type: 'paragraph', children: [{ text: '' }] }]);
};


const withButton = (editor) => {
  const { isInline } = editor;

  editor.isInline = (element) => {
    return element.type === 'button' ? true : isInline(element);
  };

  editor.insertData = (data) => {
    const text = data.getData('text/plain');
    insertButton(editor, { text, range: editor.range });
  };
  return editor;
};

export { initialButton, insertButton, withButton };
