import compose from 'compose-function';
import { withHistory } from 'slate-history';
import { withReact } from 'slate-react';


import normalize from './normalize';
import { withGrid } from './withGrid';
import { withImages, insertImage } from './withImages';
import { insertVideo, withVideo } from './withVideo';
import { insertButton, withButton } from './withButton';
import { withHtml } from './withHtml';
import { withLinks } from './withLinks';


export function withPlugins(editor) {
  return compose(
    normalize,
    // withGrid,
    // withImages,
    // withVideo,
    withLinks,
    withButton,
    withHtml,
    withReact,
    withHistory,
  )(editor);
}

export {
  withGrid,
  withHistory,
  withReact,
  withImages,
  withVideo,
  withHtml,
  withLinks,
  insertImage,
  insertVideo,
  insertButton,
};
