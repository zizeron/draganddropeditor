/* eslint-disable no-restricted-syntax */
import { Transforms, Element, Node, Path, Editor } from 'slate';


const getLastNode = (editor) => {
  const { children } = editor;
  const lastNode = children[children.length - 1];
  return lastNode;
};


const normalize = (editor) => {
  const { normalizeNode, children } = editor;
  console.log("normalize -> children", children)

  editor.normalizeNode = (entry) => {
    const [node, path] = entry;

    // If the element is a paragraph, ensure its children are valid.
    if (Element.isElement(node) && node.type === 'paragraph') {
      for (const [child, childPath] of Node.children(editor, path)) {
        if (Element.isElement(child) && !editor.isInline(child)) {
          Transforms.unwrapNodes(editor, { at: childPath });
          return;
        }
      }
    }

    // Add a paragraph block when the last node type is not paragraph
    // const lastNode = getLastNode(editor);
    // console.log("editor.normalizeNode -> lastNode", lastNode.type)
    // if (lastNode && lastNode.type !== 'paragraph') {
    //   Transforms.insertNodes(
    //     editor,
    //     {
    //       type: 'paragraph',
    //       children: [{ text: '' }],
    //     },
    //   );
    //   return;
    // }

    // Fall back to the original `normalizeNode` to enforce other constraints.
    normalizeNode(entry);
  };

  return editor;
};

export default normalize;
