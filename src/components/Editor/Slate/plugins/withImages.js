import isImage from 'is-image';
import { Editor, Transforms } from 'slate';


export const initialImage = [
  {
    type: 'div',
    className: 'image-container',
    children: [
      {
        type: 'paragraph',
        children: [{ text: '' }],
      },
    ],
  }
];


function getAboveBlockType(editor) {
  const tmp = Editor.above(editor, {
    match: (n) => Editor.isBlock(editor, n),
  });
  return tmp && tmp[0].type;
}


export const insertImage = (editor, data) => {
  const { url, caption, width = '100%' } = data;

  const image = {
    type: 'figure',
    width,
    children: [
      { type: 'image', src: url, children: [{ text: '' }] },
      { type: 'figcaption', children: [{ text: caption }] },
    ],
  };

  Transforms.insertNodes(editor, [image, { type: 'paragraph', children: [{ text: '' }] }]);
};


export function withImages(editor) {
  const { insertData, isVoid, insertBreak } = editor;

  editor.isVoid = (element) => {
    return element.type === 'div' ? true : isVoid(element);
  };

  editor.isVoid = (element) => {
    return element.type === 'figure' ? true : isVoid(element);
  };

  editor.isVoid = (element) => {
    return element.type === 'image' ? true : isVoid(element);
  };

  editor.insertBreak = () => {
    const type = getAboveBlockType(editor);
    insertBreak();
    if (type === 'figcaption') {
      Transforms.liftNodes(editor);
    }
  };

  editor.insertData = (data) => {
    const text = data.getData('text/plain');

    if (text.url && isImage(text.url)) {
      insertImage(editor, { text, range: editor.range });
    } else {
      insertData(data);
    }
  };
  return editor;
}
