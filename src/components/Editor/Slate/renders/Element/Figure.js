import React from 'react';


function Figure(props) {
  const { attributes, element = {} ,children } = props;
  const { width } = element;

  const getWidth = () => {
    if (width === 'half') return '50%';
    if (width === 'third') return '33%';
    if (width === 'two-thirds') return '66%';
    return '100%';
  };

  return (
    <figure {...attributes} style={{ margin: 0 }}>{children}</figure>
  );
}

export default Figure;
