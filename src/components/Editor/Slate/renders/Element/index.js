import React from 'react';
// import PropTypes from 'prop-types';

import Blockquote from './Blockquote';
import Figure from './Figure';
import Video from './Video';
import Link from './Link';
import Image from './Image';
import Button from './Button';
import { Grid, Container } from './Grid';


const Element = (props) => {
  const { attributes, children, element = {} } = props;
  const { className = '' } = element;
  const { type } = element;

  switch (type) {
    case 'div':
      return <div {...attributes} className={className}>{children}</div>;
    case 'heading-one':
      return <h1 {...attributes}>{children}</h1>;
    case 'heading-two':
      return <h2 {...attributes}>{children}</h2>;
    case 'heading-three':
      return <h3 {...attributes}>{children}</h3>;
    case 'heading-four':
      return <h4 {...attributes}>{children}</h4>;
    case 'heading-five':
      return <h5 {...attributes}>{children}</h5>;
    case 'heading-six':
      return <h6 {...attributes}>{children}</h6>;
    case 'blockquote':
      return <Blockquote attributes={attributes}>{children}</Blockquote>;
    case 'mark':
      return <mark {...attributes}>{children}</mark>;
    case 'hr':
      return <hr {...attributes} />;
    case 'pre':
      return <pre {...attributes}>{children}</pre>;
    case 'bulleted-list':
      return <ul {...attributes}>{children}</ul>;
    case 'numbered-list':
      return <ol {...attributes}>{children}</ol>;
    case 'list-item':
      return <li {...attributes}>{children}</li>;
    case 'video':
      return <Video {...props} />;
    case 'link':
      return <Link {...props} />;
    case 'button':
      return <Button {...props} />;
    case 'image':
      return <Image {...props} />;
    case 'figure':
      return <Figure {...props} />;
    case 'figcaption':
      return <figcaption {...attributes}>{children}</figcaption>;
    case 'grid':
      return <Grid {...props} />;
    case 'container':
      return <Container id={element.id} {...props} />;
    default:
      return <p {...attributes}>{children}</p>;
  }
};

export default Element;
