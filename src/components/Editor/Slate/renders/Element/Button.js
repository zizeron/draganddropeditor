import React from 'react';
import PropTypes from 'prop-types';


const Button = ({ attributes, children, element }) => {
  const { url } = element;

  return (
    <a
      {...attributes}
      href={url}
      style={{ border: '1px solid blue', color: 'blue', padding: '10px', margin: '10px' }}
    >
      {children}
    </a>
  );
};

Button.propTypes = {
  attributes: PropTypes.shape({
    text: PropTypes.string,
    url: PropTypes.string,
  }),
};

Button.defaultProps = {
  attributes: {
    text: '',
    url: '',
  },
};

export default Button;
