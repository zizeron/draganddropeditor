import React from 'react';


function Grid(props) {
  const { attributes, children, element } = props;

  return (
    <div {...attributes} style={{display: 'flex', justifyContent: 'space-between'}}>
      {children}
    </div>
  );
}


function Container(props) {
  const { attributes, children, element } = props;
  const width = element.columns == 2 ? '50%' : '33%';

  return (
    <div {...attributes} style={{ width, padding: '0 5px', border: '1px solid #eee' }}>
      {children}
    </div>
  );
}


export { Grid, Container };
