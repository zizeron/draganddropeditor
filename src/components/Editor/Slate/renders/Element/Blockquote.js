import React from 'react';


function Blockquote(props) {
  const { attributes, children } = props;

  return (
    <blockquote
      style={{
        borderLeft: '4px solid grey',
        paddingLeft: '5px',
      }}
      {...attributes}
    >
      {children}
    </blockquote>
  );
}

export default Blockquote;
