import React from 'react';
import PropTypes from 'prop-types';


const Link = ({ attributes, children, element }) => {
  const { title, href, target } = attributes;

  return (
    <a
      {...attributes}
      href={href}
      style={{ borderBottom: '1px solid blue', color: 'blue' }}
    >
      {children}
    </a>
  );
};

Link.propTypes = {
  attributes: PropTypes.shape({
    title: PropTypes.string,
    href: PropTypes.string,
    target: PropTypes.string,
  })
};

Link.defaultProps = {
  attributes: {
    title: '',
    href: '',
    target: '_self',
  }
};

export default Link;
