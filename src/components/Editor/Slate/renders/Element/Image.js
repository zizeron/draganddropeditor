import React from 'react';
import { useFocused, useSelected } from 'slate-react';


function Image(props) {
  const { attributes, element = {} ,children } = props;
  const selected = useSelected();
  const focused = useFocused();
  const { src, caption } = element;

  return (
    <img
      src={src}
      alt={caption}
      style={{ maxWidth: '100%', outline: selected ? '2px solid blue' : 'none' }}
    />
  );
}

export default Image;
