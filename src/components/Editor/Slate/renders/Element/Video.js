import React from 'react';
import ReactPlayer from 'react-player';


function Video({ element }) {
  const { url } = element;

  return (
    <ReactPlayer url={url} width="100%" />
  );
}

export default Video;
