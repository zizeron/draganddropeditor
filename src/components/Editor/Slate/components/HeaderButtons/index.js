import React from 'react';
import PropTypes from 'prop-types';
import { useSlate } from 'slate-react';
import { Editor, Transforms } from 'slate';

import {
  LooksOne,
  LooksTwo,
  Looks3,
  Looks4,
  Looks5,
  Looks6,
} from '@material-ui/icons';

import ToolbarButton from '../ToolbarButton';
import Icon from '../Icon';


const isBlockActive = (editor, format) => {
  const [match] = Editor.nodes(editor, {
    match: (n) => n.type === format,
  });

  return !!match;
};


const LIST_TYPES = ['numbered-list', 'bulleted-list'];

const toggleBlock = (editor, format) => {
  const isActive = isBlockActive(editor, format);
  const isList = LIST_TYPES.includes(format);

  Transforms.unwrapNodes(editor, {
    match: (n) => LIST_TYPES.includes(n.type),
    split: true,
  });

  Transforms.setNodes(editor, {
    type: isActive ? 'paragraph' : isList ? 'list-item' : format,
  });

  if (!isActive && isList) {
    const block = { type: format, children: [] };
    Transforms.wrapNodes(editor, block);
  }
};


const BlockButton = ({ format, icon }) => {
  const editor = useSlate();
  return (
    <ToolbarButton
      active={isBlockActive(editor, format)}
      onMouseDown={(event) => {
        event.preventDefault();
        toggleBlock(editor, format);
      }}
    >
      <Icon>{icon}</Icon>
    </ToolbarButton>
  );
};

BlockButton.propTypes = {
  format: PropTypes.string.isRequired,
  icon: PropTypes.node.isRequired,
};

const HeaderButtons = () => (
  <>
    <BlockButton format="heading-one" icon={<LooksOne />} />
    <BlockButton format="heading-two" icon={<LooksTwo />} />
    <BlockButton format="heading-three" icon={<Looks3 />} />
    <BlockButton format="heading-four" icon={<Looks4 />} />
    <BlockButton format="heading-five" icon={<Looks5 />} />
    <BlockButton format="heading-six" icon={<Looks6 />} />
  </>
);

export default HeaderButtons;
