
import React from 'react';
// import PropTypes from 'prop-types';
import { useSlate } from 'slate-react';
import SvgIcon from '@material-ui/core/SvgIcon';


import { insertGrid } from '../../plugins/withGrid';
import ToolbarButton from '../ToolbarButton';
import Icon from '../Icon';


const GridButtons = (props) => {
  const editor = useSlate();

  return (
    <>
      <ToolbarButton
        onMouseDown={(event) => {
          event.preventDefault();
          insertGrid(editor, 2);
        }}
      >
        <SvgIcon>
          <path fillRule="evenodd" d="M14 2H2a1 1 0 0 0-1 1v10a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V3a1 1 0 0 0-1-1zM2 1a2 2 0 0 0-2 2v10a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V3a2 2 0 0 0-2-2H2z"/>
          <path fillRule="evenodd" d="M7.5 14V2h1v12h-1z"/>
        </SvgIcon>
      </ToolbarButton>

      <ToolbarButton
        onMouseDown={(event) => {
          event.preventDefault();
          insertGrid(editor, 3);
        }}
      >
        <SvgIcon>
          <path fillRule="evenodd" d="M0 2.5A1.5 1.5 0 0 1 1.5 1h13A1.5 1.5 0 0 1 16 2.5v11a1.5 1.5 0 0 1-1.5 1.5h-13A1.5 1.5 0 0 1 0 13.5v-11zM1.5 2a.5.5 0 0 0-.5.5v11a.5.5 0 0 0 .5.5h13a.5.5 0 0 0 .5-.5v-11a.5.5 0 0 0-.5-.5h-13z"/>
          <path fillRule="evenodd" d="M5 15V1h1v14H5zm5 0V1h1v14h-1z"/>
        </SvgIcon>
      </ToolbarButton>
    </>
    // <div className="dropdown">
    //   {
    //     columns == 2 && (
    //       <ToolbarButton
    //         onMouseDown={(event) => {
    //           event.preventDefault();
    //           insertGrid(editor, 2);
    //         }}
    //       >
    //         <Icon>looks_two</Icon>
    //       </ToolbarButton>
    //     )
    //   }
    //   {
    //     columns == 3 && (
    //       <ToolbarButton
    //         onMouseDown={(event) => {
    //           event.preventDefault();
    //           insertGrid(editor, 3);
    //         }}
    //       >
    //         <Icon>looks_3</Icon>
    //       </ToolbarButton>
    //     )
    //   }
    // </div>
  );
}

export default GridButtons;
