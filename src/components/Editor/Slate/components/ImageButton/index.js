
import React, { useState } from 'react';
import { useSlate } from 'slate-react';

import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  Tabs,
  Tab,
} from '@material-ui/core';
import Image from '@material-ui/icons/Image';

import UploadImage from './UploadImage';
import ImageUrl from './ImageUrl';
import ToolbarButton from '../ToolbarButton';
import { insertImage } from '../../plugins';


const ImageFormDialog = (props) => {
  const {
    caption = '',
    onAccept = () => {},
    onChange = () => {},
    onClose = () => {},
    open = false,
    url = '',
    width = '',
  } = props;

  const [tab, setTab] = useState(0);

  return (
    <Dialog open={open} onClose={onClose} aria-labelledby="image-form-dialog-title">
      <DialogContent>
        <Tabs
          value={tab}
          onChange={(event, newValue) => setTab(newValue)}
          indicatorColor="primary"
          textColor="primary"
        >
          <Tab label="URL" />
          <Tab label="Subir imagen" />
        </Tabs>
        {
          tab === 0 && (
            <ImageUrl
              onChange={onChange}
              url={url}
              caption={caption}
              width={width}
            />
          )
        }
        {
          tab === 1 && (
            <UploadImage
              // onChange={this.onChange}
              // url={url}
              // caption={caption}
              // width={width}
              // selectOnChange={this.selectOnChange}
            />
          )
        }
      </DialogContent>
      <DialogActions>
        <Button onClick={onClose} color="secondary">
          Cancelar
        </Button>
        <Button onClick={onAccept} color="primary">
          Aceptar
        </Button>
      </DialogActions>
    </Dialog>
  );
};


const defaults = {
  open: false,
  range: null,
  isExpanded: false,
  url: '',
  caption: '',
  width: '',
};


const ImageButton = (props) => {
  const editor = useSlate();
  const [state, setState] = useState(defaults);

  const updateState = (values) => setState({ ...state, ...values });


  const handleOpen = () => {
    updateState({ open: true });
  };


  const onAccept = () => {
    const { url, caption, width } = state;

    const data = { url, caption, width };
    insertImage(editor, data);
    updateState({ open: false });
  };

  const onChange = (path, value) => {
    updateState({ [path]: value });
  };


  return (
    <>
      <ToolbarButton
        onMouseDown={(event) => {
          event.preventDefault();
          handleOpen(editor);
        }}
      >
        <Image />
      </ToolbarButton>
      <ImageFormDialog
        open={state.open}
        onAccept={onAccept}
        onClose={() => updateState({ open: false })}
        onChange={onChange}
        url={state.url}
        caption={state.caption}
        width={state.width}
      />
    </>
  );
};

export default ImageButton;
