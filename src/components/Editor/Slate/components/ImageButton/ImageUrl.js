import React from 'react';

import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  root: {
    '& .MuiTextField-root': {
      margin: theme.spacing(1),
    },
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
}));


function ImageUrl(props) {
  const { onChange, url = '', caption = '', width = '' } = props;

  const classes = useStyles();

  return (
    <div className={classes.root}>
      <div className="mini-img">
        <img src={url} alt="mini img" />
      </div>
      <TextField
        label="URL de la imagen"
        onChange={(e) => onChange('url', e.target.value)}
        name="url"
        value={url}
        variant="outlined"
        fullWidth
      />
      <TextField
        label="Texto de la imagen"
        onChange={(e) => onChange('caption', e.target.value)}
        name="caption"
        value={caption}
        variant="outlined"
        fullWidth
      />
      <TextField
        label="Tamaño de la imagen"
        value={width}
        onChange={(e) => onChange('width', e.target.value)}
        select
        variant="outlined"
        fullWidth
      >
        <MenuItem value="full">Ancho completo</MenuItem>
        <MenuItem value="half">Ancho 1/2</MenuItem>
        <MenuItem value="third">Ancho 1/3</MenuItem>
        <MenuItem value="two-thirds">Ancho 2/3</MenuItem>
      </TextField>
    </div>
  );
}

export default ImageUrl;
