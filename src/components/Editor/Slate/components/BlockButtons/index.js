import React from 'react';
import PropTypes from 'prop-types';
import { useSlate } from 'slate-react';
import { Editor, Transforms } from 'slate';

import {
  FormatQuote,
  FormatListNumbered,
  FormatListBulleted,
  TextFields,
} from '@material-ui/icons';

import ToolbarButton from '../ToolbarButton';
import Icon from '../Icon';


const isBlockActive = (editor, format) => {
  const [match] = Editor.nodes(editor, {
    match: (n) => n.type === format,
  });

  return !!match;
};


const LIST_TYPES = ['numbered-list', 'bulleted-list'];

const toggleBlock = (editor, format) => {
  const isActive = isBlockActive(editor, format);
  const isList = LIST_TYPES.includes(format);

  Transforms.unwrapNodes(editor, {
    match: (n) => LIST_TYPES.includes(n.type),
    split: true,
  });

  Transforms.setNodes(editor, {
    type: isActive ? 'paragraph' : isList ? 'list-item' : format,
  });

  if (!isActive && isList) {
    const block = { type: format, children: [] };
    Transforms.wrapNodes(editor, block);
  }
};


const BlockButton = ({ format, icon }) => {
  const editor = useSlate();
  return (
    <ToolbarButton
      active={isBlockActive(editor, format)}
      onMouseDown={(event) => {
        event.preventDefault();
        toggleBlock(editor, format);
      }}
    >
      <Icon>{icon}</Icon>
    </ToolbarButton>
  );
};

BlockButton.propTypes = {
  format: PropTypes.string.isRequired,
  icon: PropTypes.node.isRequired,
};

const BlockButtons = () => (
  <>
    <BlockButton format="blockquote" icon={<FormatQuote />} />
    <BlockButton format="numbered-list" icon={<FormatListNumbered />} />
    <BlockButton format="bulleted-list" icon={<FormatListBulleted />} />
  </>
);

export default BlockButtons;
