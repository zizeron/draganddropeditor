import React from 'react';


const ToolbarButton = React.forwardRef(
  ({ className, active, ...props }, ref) => (
    <span
      {...props}
      ref={ref}
      className={`editor-toolbar-btn ${active ? 'editor-toolbar-btn-active' : ''}`}
    />
  )
);

export default ToolbarButton;
