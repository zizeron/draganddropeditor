
import React, { useState } from 'react';
import { Editor, Range, Node } from 'slate';
import { useSlate } from 'slate-react';
import { makeStyles } from '@material-ui/core/styles';

import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  TextField,
  MenuItem,
} from '@material-ui/core';
import LinkIcon from '@material-ui/icons/Link';

import { findLink, isLinkActive, wrapLink, unwrapLink } from './helpers';
import ToolbarButton from '../ToolbarButton';


const useStyles = makeStyles((theme) => ({
  root: {
    '& .MuiTextField-root': {
      margin: theme.spacing(1),
    },
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
}));


const LinkFormDialog = (props) => {
  const {
    attributes = [],
    onAccept = () => {},
    onClose = () => {},
    onRemove = () => {},
    open = false,
    text = '',
    updateAttribute = '',
    updateText = '',
  } = props;

  const classes = useStyles();

  return (
    <Dialog open={open} onClose={onClose} aria-labelledby="link-form-dialog-title">
      <DialogTitle id="link-form-dialog-title">Insertar/editar enlace</DialogTitle>
      <DialogContent className={classes.root}>
        <TextField
          label="Texto del enlace"
          value={text}
          onChange={(e) => updateText(e.target.value)}
          fullWidth
          variant="outlined"
        />
        <TextField
          label="Título alternativo del enlace"
          value={attributes.title}
          onChange={(e) => updateAttribute('title', e.target.value)}
          fullWidth
          variant="outlined"
        />
        <TextField
          label="Dirección del enlace"
          value={attributes.href}
          onChange={(e) => updateAttribute('href', e.target.value)}
          fullWidth
          variant="outlined"
        />
        <TextField
          label="Elegir apertura en ventana:"
          value={attributes.target}
          onChange={(e) => updateAttribute('target', e.target.value)}
          select
          fullWidth
          variant="outlined"
        >
          <MenuItem value="_self">Misma ventana</MenuItem>
          <MenuItem value="_blank">Ventana nueva</MenuItem>
        </TextField>
      </DialogContent>
      <DialogActions>
        {onRemove && (
          <Button onClick={onRemove} color="secondary">
            Eliminar enlace
          </Button>
        )}
        <Button onClick={onClose} color="primary">
          Cancelar
        </Button>
        <Button onClick={onAccept} color="primary">
          Aceptar
        </Button>
      </DialogActions>
    </Dialog>
  );
};


const getInitialLinkData = (editor) => {
  const link = findLink(editor);

  const isExpanded = editor.selection ? Range.isExpanded(editor.selection) : false;

  const text = editor.selection && isExpanded
    ? Editor.string(editor, editor.selection)
    : (link && Node.string(link)) || '';

  return {
    isExpanded,
    link,
    text,
    range: editor.selection ? { ...editor.selection } : null,
    attributes: link ? link.attributes : {},
  };
};


const defaults = {
  open: false,
  range: null,
  isExpanded: false,
  link: null,
  text: '',
  attributes: {},
};


const LinkButton = (props) => {
  const editor = useSlate();
  const [state, setState] = useState(defaults);

  const updateState = (values) => setState({ ...state, ...values });


  const handleOpen = () => {
    const linkData = getInitialLinkData(editor);
    updateState({ open: true, ...linkData });
  };


  const updateAttribute = (name, value) => {
    updateState({ attributes: { ...state.attributes, [name]: value } });
  };


  const cleanAttributesMutate = (attributes) => {
    Object.entries(attributes).forEach(([key, value]) => {
      return (value === null || value === undefined) && delete (attributes)[key];
    });
  };


  const onAccept = () => {
    const { text, attributes } = state;
    cleanAttributesMutate(attributes);

    if (!state.range) {
      throw new Error('Invalid range. Must be typeof Range.');
    }

    const command = { attributes, text, range: state.range };
    wrapLink(editor, command);
    updateState({ open: false });
  };


  const onRemove = () => {
    unwrapLink(editor);
    updateState({ open: false });
  };


  return (
    <>
      <ToolbarButton
        active={isLinkActive(editor)}
        onMouseDown={(event) => {
          event.preventDefault();
          handleOpen(editor);
        }}
      >
        <LinkIcon />
      </ToolbarButton>
      <LinkFormDialog
        open={state.open}
        attributes={state.attributes}
        text={state.text}
        updateText={(text) => updateState({ text })}
        updateAttribute={updateAttribute}
        onRemove={onRemove}
        onAccept={onAccept}
        onClose={() => updateState({ open: false })}
      />
    </>
  );
};

export default LinkButton;
