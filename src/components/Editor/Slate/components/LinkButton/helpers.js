import { Editor, Range, Transforms } from 'slate';


const LINK_TAG = 'a';
const match = (n = {}) => n.type === 'link';


const findLinkEntry = (editor) => {
  const [link] = Editor.nodes(editor, { match });
  return link;
};


const findLink = (editor) => {
  const linkEntry = findLinkEntry(editor);
  return linkEntry ? linkEntry[0] : null;
};


const isLinkActive = (editor) => {
  return !!findLink(editor);
};


const unwrapLink = (editor) => {
  Transforms.unwrapNodes(editor, { match: (n) => n.type === 'link' });
};


const wrapLink = (editor, command) => {
  const { range, attributes, text } = command;
  const foundLinkEntry = findLinkEntry(editor);
  Transforms.setSelection(editor, range);
  const isCollapsed = range && Range.isCollapsed(range);

  const link = {
    tag: LINK_TAG,
    type: 'link',
    attributes,
    children: [{ text }],
  };

  if (!foundLinkEntry && isCollapsed) {
    Transforms.insertNodes(editor, link, { at: range });
  } else {
    if (isCollapsed) {
      const path = foundLinkEntry[1];
      Transforms.setNodes(editor, link, { at: path, split: true });
    } else {
      Transforms.wrapNodes(editor, link, { at: range, split: true });
    }
    Transforms.collapse(editor, { edge: 'end' });
  }
};


export {
  findLink,
  isLinkActive,
  unwrapLink,
  wrapLink,
};
