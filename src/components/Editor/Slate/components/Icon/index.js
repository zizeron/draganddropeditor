import React from 'react';


const Icon = React.forwardRef(({ className, ...props }, ref) => (
  <span
    {...props}
    ref={ref}
    className="editor-toolbar-icon"
  />
));

export default Icon;
