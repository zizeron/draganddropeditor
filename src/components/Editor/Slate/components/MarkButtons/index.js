import React from 'react';
import { Editor } from 'slate';
import { useSlate } from 'slate-react';

import {
  FormatBold,
  FormatItalic,
  FormatUnderlined,
  Code,
} from '@material-ui/icons';

import ToolbarButton from '../ToolbarButton';
import Icon from '../Icon';


const isMarkActive = (editor, format) => {
  const marks = Editor.marks(editor);
  return marks ? marks[format] === true : false;
};


const toggleMark = (editor, format) => {
  const isActive = isMarkActive(editor, format);

  if (isActive) {
    Editor.removeMark(editor, format);
  } else {
    Editor.addMark(editor, format, true);
  }
};


const MarkButton = ({ format, icon }) => {
  const editor = useSlate();

  return (
    <ToolbarButton
      active={isMarkActive(editor, format)}
      onMouseDown={(event) => {
        event.preventDefault();
        toggleMark(editor, format);
      }}
    >
      <Icon>{icon}</Icon>
    </ToolbarButton>
  );
};


const MarkButtons = () => {
  return (
    <>
      <MarkButton format="bold" icon={<FormatBold />} />
      <MarkButton format="italic" icon={<FormatItalic />} />
      <MarkButton format="underline" icon={<FormatUnderlined />} />
      <MarkButton format="code" icon={<Code />} />
    </>
  );
};

export default MarkButtons;
