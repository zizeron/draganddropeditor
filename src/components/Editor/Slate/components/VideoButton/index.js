
import React, { useState } from 'react';
import { useSlate } from 'slate-react';
import { makeStyles } from '@material-ui/core/styles';

import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  TextField,
} from '@material-ui/core';
import PlayArrow from '@material-ui/icons/PlayArrow';

import ToolbarButton from '../ToolbarButton';
import { insertVideo } from '../../plugins';


const useStyles = makeStyles((theme) => ({
  root: {
    '& .MuiTextField-root': {
      margin: theme.spacing(1),
    },
    '& .MuiPaper-root': {
      minWidth: 300,
    },
  },
  formControl: {
    margin: theme.spacing(1),
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  }
}));


const VideoFormDialog = (props) => {
  const {
    onAccept = () => {},
    onChange = () => {},
    onClose = () => {},
    open = false,
    url = '',
  } = props;

  const classes = useStyles();

  return (
    <Dialog open={open} onClose={onClose} aria-labelledby="video-form-dialog-title">
      <DialogTitle id="video-form-dialog-title">Insertar enlace del vídeo</DialogTitle>
      <DialogContent className={classes.root}>
        <TextField
          label="URL del vídeo"
          onChange={(e) => onChange('url', e.target.value)}
          value={url}
          name="url"
          fullWidth
          variant="outlined"
        />
      </DialogContent>
      <DialogActions>
        <Button onClick={onClose} color="secondary">
          Cancelar
        </Button>
        <Button onClick={onAccept} color="primary">
          Aceptar
        </Button>
      </DialogActions>
    </Dialog>
  );
};


const defaults = {
  open: false,
  range: null,
  isExpanded: false,
  url: '',
};

const VideoButton = (props) => {
  const editor = useSlate();
  const [state, setState] = useState(defaults);

  const updateState = (values) => setState({ ...state, ...values });


  const onChange = (path, value) => {
    updateState({ [path]: value });
  };


  const handleOpen = () => {
    updateState({ open: true });
  };


  const onAccept = () => {
    const { url } = state;
    insertVideo(editor, url);
    updateState({ open: false });
  };


  return (
    <>
      <ToolbarButton
        onMouseDown={(event) => {
          event.preventDefault();
          handleOpen(editor);
        }}
      >
        <PlayArrow />
      </ToolbarButton>
      <VideoFormDialog
        open={state.open}
        onAccept={onAccept}
        onChange={onChange}
        onClose={() => updateState({ open: false })}
        url={state.url}
      />
    </>
  );
};

export default VideoButton;
