import React, { useCallback, useMemo, useState, useRef, useEffect } from 'react';
import PropTypes from 'prop-types';
import { createEditor } from 'slate';
import { Editable, Slate } from 'slate-react';

import { withPlugins } from './plugins';
import initialValue from './helpers/initialValue';
import Element from './renders/Element';
import Leaf from './renders/Leaf';
import { Toolbar, MarkButtons, BlockButtons } from './components';
import hotKeysFunction from './components/HotKeys';
import LinkButton from './components/LinkButton';
import VideoButton from './components/VideoButton';
import ImageButton from './components/ImageButton';
import ButtonButton from './components/ButtonButton';
import HeaderButtons from './components/HeaderButtons';
import GridButtons from './components/GridButtons';
import { gridTwo, gridThree } from './plugins/withGrid';
import { initialImage } from './plugins/withImages';
import { initialButton } from './plugins/withButton';


const SlateEditor = (props) => {
  const {
    all = false,
    blockButtons = false,
    blockData,
    buttonButton = false,
    columnsThree = false,
    columnsTwo = false,
    headerButtons = false,
    imageButton = false,
    layoutBlocks,
    linkButton = false,
    markButtons = false,
    onKeyDown = false,
    readOnly,
    videoButton = false,
  } = props;


  const getInitialValue = () => {
    if (blockData.data) return blockData.data;
    if (columnsTwo) return [gridTwo];
    if (columnsThree) return [gridThree];
    if (imageButton) return initialImage;
    if (buttonButton) return initialButton;
    return initialValue;
  };

  const defaultValue = getInitialValue();

  const [value, setValue] = useState(defaultValue);
  // console.log("SlateEditor -> value", value)
  const renderElement = useCallback((elProps) => <Element {...elProps} />, []);
  const renderLeaf = useCallback((leafProps) => <Leaf {...leafProps} />, []);
  const editor = useMemo(() => withPlugins(createEditor()), []);


  const onChange = (newValue) => {
    setValue(newValue);
  };


  const [changedBlock, setChangedBlock] = useState(blockData);
  const oldData = useRef(blockData);

  useEffect(() => {
    setChangedBlock({
      ...blockData,
      data: value,
    });
    oldData.current = { ...changedBlock };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [value]);


  useEffect(() => {
    layoutBlocks.forEach((block, i) => {
      if (block.id === changedBlock.id) {
        layoutBlocks.splice(i, 1, changedBlock);
      }
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [changedBlock]);


  return (
    <div className={`editor ${readOnly ? 'editor-readonly' : 'editor-editable'}`}>
      <Slate
        editor={editor}
        value={value}
        onChange={(value) => onChange(value)}
      >
        {
          readOnly ? (
            <Editable
              className="iam-react-editor"
              renderElement={renderElement}
              renderLeaf={renderLeaf}
              readOnly
            />
          ) : (
            <>
              <Toolbar>
                { headerButtons && <HeaderButtons /> }
                { markButtons && <MarkButtons /> }
                { blockButtons && <BlockButtons /> }
                { linkButton && <LinkButton /> }
                { imageButton && <ImageButton /> }
                { videoButton && <VideoButton /> }
                { buttonButton && <ButtonButton /> }
                { (columnsTwo || columnsThree) && (
                  <>
                    <MarkButtons />
                    <BlockButtons />
                    <LinkButton />
                    <ImageButton />
                    <VideoButton />
                  </>
                )}

                {
                  all && (
                    <>
                      <HeaderButtons />
                      <MarkButtons />
                      <BlockButtons />
                      <LinkButton />
                      <ImageButton />
                      <VideoButton />
                      <ButtonButton />
                      <GridButtons />
                    </>
                  )
                }
              </Toolbar>

              <Editable
                className="iam-react-editor"
                renderElement={renderElement}
                renderLeaf={renderLeaf}
                placeholder="Enter some rich text…"
                spellCheck
                autoFocus
                onKeyDown={onKeyDown ? (event) => hotKeysFunction(event, editor) : false}
              />
            </>
          )
        }
      </Slate>
    </div>
  );
};

SlateEditor.propTypes = {
  all: PropTypes.bool,
  blockButtons: PropTypes.bool,
  blockData: PropTypes.object.isRequired,
  buttonButton: PropTypes.bool,
  columnsThree: PropTypes.bool,
  columnsTwo: PropTypes.bool,
  headerButtons: PropTypes.bool,
  imageButton: PropTypes.bool,
  layoutBlocks: PropTypes.array.isRequired,
  linkButton: PropTypes.bool,
  markButtons: PropTypes.bool,
  onKeyDown: PropTypes.bool,
  readOnly: PropTypes.bool,
  videButton: PropTypes.bool,
  videoButton: PropTypes.bool,
};

SlateEditor.defaultProps = {
  all: false,
  blockButtons: false,
  buttonButton: false,
  columnsThree: false,
  columnsTwo: false,
  headerButtons: false,
  imageButton: false,
  linkButton: false,
  markButtons: false,
  onKeyDown: false,
  readOnly: false,
  videButton: false,
  videoButton: false,
};

export default SlateEditor;
