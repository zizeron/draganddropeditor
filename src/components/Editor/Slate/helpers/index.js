import { Editor, Range, Transforms } from 'slate';


const isMarkActive = (editor, format) => {
  const marks = Editor.marks(editor);
  return marks ? marks[format] === true : false;
};


const isLinkActive = (editor) => {
  const [link] = Editor.nodes(editor, { match: n => n.type === 'link' })
  return !!link;
};


const unwrapLink = (editor) => {
  Transforms.unwrapNodes(editor, { match: n => n.type === 'link' })
};


const wrapLink = (editor, url) => {
  if (isLinkActive(editor)) {
    unwrapLink(editor);
  }

  const { selection } = editor;
  const isCollapsed = selection && Range.isCollapsed(selection);
  const link = {
    type: 'link',
    url,
    children: isCollapsed ? [{ text: url }] : [],
  };

  if (isCollapsed) {
    Transforms.insertNodes(editor, link);
  } else {
    Transforms.wrapNodes(editor, link, { split: true });
    Transforms.collapse(editor, { edge: 'end' });
  }
};


const insertLink = (editor, url) => {
  if (editor.selection) {
    wrapLink(editor, url);
  }
};


const insertImage = (editor, attributes) => {
  const text = { text: '' };
  const image = { type: 'image', attributes, children: [text] }
  Transforms.insertNodes(editor, [image, { type: 'paragraph', children: [{ text: '' }] }]);
}


export {
  isMarkActive,
  insertLink,
  isLinkActive,
  unwrapLink,
  wrapLink,
  insertImage
};
