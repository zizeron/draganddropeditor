import React, { useState } from 'react';

import CssBaseline from '@material-ui/core/CssBaseline';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';

import Blockinput from './Blockinput';
import LayoutEdition from './LayoutEdition';
import { contentCreatorStyles } from './styles';


function ContentCreator() {
  const defaultValue = JSON.parse(localStorage.getItem('iam-content')) || [];

  const [blockList, setBlockList] = useState(defaultValue);
  const [num, setNum] = useState(null);


  const updateBlockList = (block) => {
    const blockNum = blockList.length;
    setNum(blockNum);
    setBlockList([...blockList, { id: Date.now(), type: block }]);
  };


  const updateOrderList = (orderedlist) => {
    setBlockList(orderedlist);
  };


  const onRemoveBlock = (index) => {
    const newBlockList = [...blockList];
    newBlockList.splice(index, 1);
    setBlockList(newBlockList);
  };


  const onSubmit = (e) => {
    e.preventDefault();
    const content = JSON.stringify(blockList);
    localStorage.setItem('iam-content', content);
    setNum(null);
  };


  const classes = contentCreatorStyles();

  return (
    <div className={classes.root}>
      <CssBaseline />
      <Container maxWidth="lg" className={classes.container}>
        <Grid container spacing={3}>

          <Grid item xs={12} md={8} lg={10}>
            <Paper className={classes.paper}>
              <LayoutEdition
                blockList={blockList}
                num={num}
                setNum={setNum}
                onSubmit={onSubmit}
                updateOrderList={updateOrderList}
                onRemoveBlock={onRemoveBlock}
              />
            </Paper>
          </Grid>

          <Grid item xs={12} md={4} lg={2}>
            <Paper className={classes.paper}>
              <Blockinput
                updateBlockList={updateBlockList}
              />
            </Paper>
          </Grid>

        </Grid>

      </Container>
    </div>
  );
}

export default ContentCreator;
