import React, { useState } from 'react';

import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';

import PreMadeInputs from '../preMadeInputs';


const LayoutEdition = (props) => {
  const { blockList, num, setNum, onSubmit, updateOrderList, onRemoveBlock } = props;
  const [draggedItem, setDraggedItem] = useState({});
  const [draggedOverItem, setDraggedOverItem] = useState({});


  const onDragStart = (index) => {
    const dragged = blockList[index];
    setDraggedItem(dragged);
  };


  const onDragOver = (index) => {
    const draggedOver = blockList[index];
    setDraggedOverItem(draggedOver);

    // if the item is dragged over itself, ignore
    if (draggedItem === draggedOverItem) {
      return;
    }

    // filter out the currently dragged item
    const currentList = blockList.filter((item) => item !== draggedItem);

    // add the dragged item after the dragged over item
    currentList.splice(index, 0, draggedItem);

    updateOrderList(currentList);
  };


  return (
    <form onSubmit={(e) => onSubmit(e)}>
      <Grid container spacing={2}>
        {
          blockList && blockList.map((item, index) => {
            const edit = index === num;

            if (edit || num !== null) {
              return (
                <PreMadeInputs
                  key={item.id}
                  inputData={item}
                  layoutList={blockList}
                  edit={edit}
                  index={index}
                  setNum={setNum}
                />
              );
            }
            return (
              <Grid
                item
                xs={12}
                key={item ? item.id : 0}
                draggable
                onDragOver={() => onDragOver(index)}
                onDragStart={() => onDragStart(index)}
                className="dragabble-editor"
              >
                <PreMadeInputs
                  key={item.id}
                  inputData={item}
                  layoutList={blockList}
                  edit={edit}
                  index={index}
                  setNum={setNum}
                  onRemoveBlock={onRemoveBlock}
                />
              </Grid>
            );
          })
        }
      </Grid>

      <Grid container spacing={2} style={{ marginTop: '100px' }}>
        <Button
          size="small"
          type="submit"
          color="primary"
          variant="outlined"
        >
          Guardar
        </Button>
      </Grid>

    </form>
  );
};

export default LayoutEdition;
