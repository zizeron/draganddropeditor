import { makeStyles } from '@material-ui/core/styles';


const contentCreatorStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },
  content: {
    flexGrow: 1,
    minHeight: '100vh',
  },
  container: {
    paddingTop: theme.spacing(4),
    paddingBottom: theme.spacing(4),
  },
  paper: {
    padding: theme.spacing(2),
    // display: 'flex',
    minHeight: '80vh',
    // flexDirection: 'column',
  },
}));

export { contentCreatorStyles };
