import React from 'react';

import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';


const Blockinput = ({ updateBlockList }) => {
  const onSelect = (type) => {
    updateBlockList(type);
  };

  return (
    <Grid container spacing={2}>
      <Grid item xs={12}>
        <Button
          size="small"
          variant="outlined"
          color="primary"
          onClick={() => onSelect('all')}
        >
          Editor completo
        </Button>
      </Grid>

      <Grid item xs={12}>
        <Button
          size="small"
          variant="outlined"
          color="primary"
          onClick={() => onSelect('title')}
        >
          Título
        </Button>
      </Grid>

      <Grid item xs={12}>
        <Button
          size="small"
          variant="outlined"
          color="primary"
          onClick={() => onSelect('text')}
        >
          Texto
        </Button>
      </Grid>

      <Grid item xs={12}>
        <Button
          size="small"
          variant="outlined"
          color="primary"
          onClick={() => onSelect('image')}
        >
          Imagen
        </Button>
      </Grid>

      <Grid item xs={12}>
        <Button
          size="small"
          variant="outlined"
          color="primary"
          onClick={() => onSelect('video')}
        >
          Vídeo
        </Button>
      </Grid>

      <Grid item xs={12}>
        <Button
          size="small"
          variant="outlined"
          color="primary"
          onClick={() => onSelect('button')}
        >
          Botón
        </Button>
      </Grid>

      <Grid item xs={12}>
        <Button
          size="small"
          variant="outlined"
          color="primary"
          onClick={() => onSelect('columnsTwo')}
        >
          2 columnas
        </Button>
      </Grid>

      <Grid item xs={12}>
        <Button
          size="small"
          variant="outlined"
          color="primary"
          onClick={() => onSelect('columnsThree')}
        >
          3 columnas
        </Button>
      </Grid>
    </Grid>
  );
};

export default Blockinput;
