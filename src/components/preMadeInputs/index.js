import React from 'react';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';

import Editor from '../Editor';


const PreMadeInputs = (props) => {
  const { inputData, layoutList, edit, index, setNum, onRemoveBlock } = props;

  const getEditor = () => {
    let inputTemplate;

    switch (inputData.type) {
      case 'all':
        inputTemplate = (
          <Editor
            key={inputData.id}
            blockData={inputData}
            layoutBlocks={layoutList}
            all
            onKeyDown
            edit={edit}
          />
        );
        break;

      case 'title':
        inputTemplate = (
          <Editor
            key={inputData.id}
            blockData={inputData}
            layoutBlocks={layoutList}
            headerButtons
            edit={edit}
          />
        );
        break;

      case 'text':
        inputTemplate = (
          <Editor
            key={inputData.id}
            blockData={inputData}
            layoutBlocks={layoutList}
            markButtons
            blockButtons
            linkButton
            edit={edit}
          />
        );
        break;

      case 'image':
        inputTemplate = (
          <Editor
            key={inputData.id}
            blockData={inputData}
            layoutBlocks={layoutList}
            imageButton
            edit={edit}
          />
        );
        break;

      case 'video':
        inputTemplate = (
          <Editor
            key={inputData.id}
            blockData={inputData}
            layoutBlocks={layoutList}
            videoButton
            edit={edit}
          />
        );
        break;

      case 'button':
        inputTemplate = (
          <Editor
            key={inputData.id}
            blockData={inputData}
            layoutBlocks={layoutList}
            buttonButton
            edit={edit}
          />
        );
        break;

      case 'columnsTwo':
        inputTemplate = (
          <Editor
            key={inputData.id}
            blockData={inputData}
            layoutBlocks={layoutList}
            columnsTwo
            edit={edit}
          />
        );
        break;

      case 'columnsThree':
        inputTemplate = (
          <Editor
            key={inputData.id}
            blockData={inputData}
            layoutBlocks={layoutList}
            columnsThree
            edit={edit}
          />
        );
        break;

      default:
        inputTemplate = 'failed';
        break;
    }

    return inputTemplate;
  };


  return (
    <Grid
      item
      xs={12}
      className={edit ? '' : 'editor-readonly-container'}
    >
      {getEditor()}
      {
        !edit && (
          <div className="btn-editar">
            <Button
              onClick={() => onRemoveBlock(index)}
              type="button"
              color="secondary"
            >
              Borrar
            </Button>

            <Button
              onClick={() => setNum(index)}
              type="button"
              color="primary"
            >
              Editar
            </Button>
          </div>
        )
      }
    </Grid>
  );
};

export default PreMadeInputs;
